#!/bin/bash

ls -lha /home/ | awk {' print $9'} | cut -d "/" -f 1 > users.txt

while read line
do
        if [[ $line == itans ]]
        then
                chown -R $line:its  /home/$line
        elif [[ $line == itunix ]]
        then
                chown -R $line:its  /home/$line
        elif [[ $line == _qualys_linux ]]
        then
                chown -R $line:its  /home/$line
#       elif [[ $line == itans || itunix || _qualys_linux ]]
#       then
#               chown -R $line:its  /home/$line
        elif [[ $line == pam_umz_reconcile ]]
        then
                chown -R $line:users  /home/$line
        elif [[ $line == addm_scan_unix ]]
        then
                chown -R $line:addm  /home/$line
        elif [[ $line == lost+found ]]
        then
                chown -R root:root  /home/$line
        else
                chown -R $line:dusers /home/$line
        fi
done < users.txt

